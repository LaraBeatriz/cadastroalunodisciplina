package model;

import java.util.ArrayList;

public class Disciplina{

	//Atributos
	private String nome;
	private String codigoDisciplina;
	private ArrayList<Aluno> listaAlunosDisciplina;
	

	//Contrutor
	//public Disciplina(String nome, String codigoDisciplina){
	public Disciplina(){	
		listaAlunosDisciplina = new ArrayList<Aluno>();
		//nome = umNome;
		//codigoDisciplina = umCodigoDisciplina;
	}

	//Métodos
	public void setNome(String umNome){
		nome = umNome;
	}

	public String getNome(){
		return nome;
	}

	public void setCodigoDisciplina(String umCodigoDisciplina){
		codigoDisciplina = umCodigoDisciplina; 
	}


	public String getCodigoDisciplina(){
		return codigoDisciplina;
	}
	
	public void adicionar(Aluno umAluno){
		listaAlunosDisciplina.add(umAluno);
		System.out.println("Aluno adicionado à disciplina!");
	}

	public void remover(Aluno umAluno){
		listaAlunosDisciplina.remove(umAluno);
		System.out.println("Aluno removido da disciplina");
	}

	public void mostrarAlunosDisciplina(){
		for(Aluno aluno:listaAlunosDisciplina){
			System.out.println(aluno.getNome());
			}
		}
	
		
		
	} 

