package view;
import java.io.*;

import controller.ControleAluno;
import controller.ControleDisciplina;

import model.Aluno;
import model.Disciplina;

public class CadastroAlunoDisciplina{

 	public static void main(String[] args) throws IOException{
    
		//burocracia para leitura de teclado
		InputStream entradaSistema = System.in;
		InputStreamReader leitor = new InputStreamReader(entradaSistema);
		BufferedReader leitorEntrada = new BufferedReader(leitor);
		String entradaTeclado;

		//instanciando objetos do sistema
		Aluno umAluno = new Aluno();
		Disciplina umaDisciplina = new Disciplina();
		ControleAluno umControleAluno = new ControleAluno();
		ControleDisciplina umControleDisciplina = new ControleDisciplina();
		String umNome,umaMatricula, nomeRemover, nomePesquisa, nomeDisciplina, nomeRemoverDisciplina, nomePesquisaDisciplina;

		//Interagindo com o usuário
		int opcao;
		do{
			System.out.println("======Menu=====");
		    System.out.println("1 - Adicionar Aluno");
		    System.out.println("2 - Remover Aluno");
		    System.out.println("3 - Pesquisar Aluno ");
		    System.out.println("4 - Adicionar Disciplina ");
		    System.out.println("5 - Remover Disciplina ");
		    System.out.println("6 - Mostrar Lista de Alunos da Disciplina ");
		    opcao = Integer.parseInt(leitorEntrada.readLine());

		    switch(opcao){
		    	case 1:
			    	System.out.println("Digite o nome do aluno:");
			    	entradaTeclado = leitorEntrada.readLine();
			        umNome = entradaTeclado;
			        umAluno.setNome(umNome);

			        System.out.println("Digite a matricula do aluno:");
			    	entradaTeclado = leitorEntrada.readLine();
			        umaMatricula = entradaTeclado;
			        umAluno.setMatricula(umaMatricula);

			        umControleAluno.addAluno(umAluno);
		        	break;
		  
		    	case 2:
			    	System.out.println("Digite o nome do aluno para remover:");
			        entradaTeclado = leitorEntrada.readLine();
			        nomeRemover = entradaTeclado;
			        umControleAluno.rmAluno(umControleAluno.pesquisar(nomeRemover));

			        umControleAluno.rmAluno(umAluno);
			        break;

		        case 3:
			        System.out.println("Digite o nome do aluno para pesquisar:");
			        entradaTeclado = leitorEntrada.readLine();
			        nomePesquisa = entradaTeclado;

			        umAluno = umControleAluno.pesquisar(nomePesquisa);
			        
			        if (umAluno != null){
			          System.out.println("\nNome:" + umAluno.getNome() 
			          + "\nMatricula:" + umAluno.getMatricula());
			         
			        }

			        break;

		        case 4:
			        System.out.println("Digite o nome da Disciplina:");
			        entradaTeclado = leitorEntrada.readLine();
			        nomeDisciplina = entradaTeclado;
			        umaDisciplina.setNome(nomeDisciplina);

			        System.out.println("Digite o codigo da Disciplina:");
			        entradaTeclado = leitorEntrada.readLine();
			        codigoDisciplina = entradaTeclado;
			        umaDisciplina.setCodigoDisciplina(codigoDisciplina);

			        umControleDisciplina.addDisciplina(umaDisciplina);

			        break;

		        case 5:
				    System.out.println("Digite o nome do aluno para remover:");
				    entradaTeclado = leitorEntrada.readLine();
				    nomeRemoverDisciplina = entradaTeclado;
				    umControleDisciplina.rmDisciplina(umControleDisciplina.pesquisar(nomeRemoverDisciplina));

				    umControleDisciplina.rmDisciplina(umaDisciplina);

			        break;

		        case 6:
			        System.out.println("Digite o nome da Disciplina:");
			        entradaTeclado = leitorEntrada.readLine();
			        nomePesquisaDisciplina = entradaTeclado;
			        umControleDisciplina.pesquisar(nomePesquisaDisciplina);

			        umaDisciplina = umControleDisciplina.pesquisar(nomePesquisaDisciplina);

			        if (umaDisciplina != null){
			        	umaDisciplina.mostrarAlunosDisciplina();
			        }
		        	break;

		        default: 
		          System.out.println("Opcao invalida!");
		          break;
			}

		}while(opcao != 0);
	}
}