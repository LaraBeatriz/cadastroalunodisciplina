package test;

import static org.junit.Assert.*;

import model.Disciplina;

import org.junit.Before;
import org.junit.Test;

import controller.ControleDisciplina;

public class ControleDisciplinaTest {
	
	private static ControleDisciplina meuControleDisciplina;
	private static Disciplina umaDisciplina;

	@Before
	public void setUp() {
		meuControleDisciplina = new ControleDisciplina();
		umaDisciplina = new Disciplina();
		
	}

	@Test
	public void testAddDisciplina() {
		umaDisciplina.setNome("Materia");
		meuControleDisciplina.addDisciplina(umaDisciplina);
		assertEquals(umaDisciplina,meuControleDisciplina.pesquisar("Materia") );
	}

	@Test
	public void testPesquisar() {
		umaDisciplina.setNome("Materia2");
		meuControleDisciplina.addDisciplina(umaDisciplina);
		assertEquals(umaDisciplina,meuControleDisciplina.pesquisar("Materia2") );
	}
	
	@Test
	public void testRmDisciplina() {
		meuControleDisciplina.rmDisciplina("Materia");
		assertEquals(null,meuControleDisciplina.pesquisar("Materia") );
	
	}

}

