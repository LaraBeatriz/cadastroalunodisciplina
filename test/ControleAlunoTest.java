package test;

import static org.junit.Assert.*;

import model.Aluno;

import org.junit.Before;
import org.junit.Test;

import controller.ControleAluno;

public class ControleAlunoTest {
	
	private ControleAluno meuControleAluno;
	private Aluno umAluno;

	@Before
	public void setUp() throws Exception {
		meuControleAluno = new ControleAluno();
		umAluno = new Aluno();	
	}
	
	@Test
	public void testAddAluno() {
		umAluno.setNome("Fulano");
		meuControleAluno.addAluno(umAluno);
		assertEquals(umAluno,meuControleAluno.pesquisar("Fulano") );
	}

	@Test
	public void testPesquisar() {
		umAluno.setNome("Fulano2");
		meuControleAluno.addAluno(umAluno);
		assertEquals(umAluno,meuControleAluno.pesquisar("Fulano2") );
	}
	
	@Test
	public void testRmAluno() {
		meuControleAluno.rmAluno("Fulano");
		assertEquals(null,meuControleAluno.pesquisar("Fulano") );
	}


}
