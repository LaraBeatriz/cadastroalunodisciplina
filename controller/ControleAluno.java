package controller;
import java.util.ArrayList;

import model.Aluno;

public class ControleAluno{
	//Atributos
	private ArrayList<Aluno>listaAlunos;

	//Construtor
	public ControleAluno(){
		listaAlunos = new ArrayList<Aluno>();
	}
	
	//Metodos
	public void addAluno(Aluno umAluno){
		listaAlunos.add(umAluno);
		System.out.println("Aluno adicionado!");
		
	}
	public void rmAluno(String umAluno){
		listaAlunos.remove(umAluno);
		System.out.println("Aluno removido!");
	}

	public Aluno pesquisar(String nome){
		for(Aluno aluno:listaAlunos){
			if(aluno.getNome().equalsIgnoreCase(nome)){
				return aluno;
			}
		}return null;
	}
}
