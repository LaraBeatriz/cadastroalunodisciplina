package controller;
import java.util.ArrayList;

import model.Disciplina;

public class ControleDisciplina{
	//Atributos
	private ArrayList<Disciplina>listaDisciplinas;
	
	//Construtor
	public ControleDisciplina(){
		listaDisciplinas = new ArrayList<Disciplina>();
	}

	//Metodos
	public void addDisciplina(Disciplina umaDisciplina){
		listaDisciplinas.add(umaDisciplina);
		System.out.println("Disciplina adicionada!");
		
	}
	public void rmDisciplina(String umaDisciplina){
		listaDisciplinas.remove(umaDisciplina);
		System.out.println("Disciplina removida!");
	}

	public Disciplina pesquisar(String nome){
		for(Disciplina disciplina:listaDisciplinas){
			if(disciplina.getNome().equalsIgnoreCase(nome)){
				return disciplina;
			}
		}return null;
	}


}
